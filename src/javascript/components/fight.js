import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    const firstIndicator = document.getElementById('left-fighter-indicator').getBoundingClientRect().width;
    const secondIndicator = document.getElementById('right-fighter-indicator').getBoundingClientRect().width;

    const message_left = document.getElementById('message_left');
    const message_right = document.getElementById('message_right');

    const playerOne = {
      ...firstFighter,
      position: 'left',
      indicator: firstIndicator,
      statusBlock: false,
      combo: [],
      timeCombo: null,
      criticalHitCombination: controls.PlayerOneCriticalHitCombination
    }
    const  playerTwo = {
      ...secondFighter,
      position: 'right',
      indicator: secondIndicator,
      statusBlock: false,
      combo: [],
      timeCombo: null,
      criticalHitCombination: controls.PlayerTwoCriticalHitCombination
    }

    document.addEventListener('keydown', downKey);
    document.addEventListener('keyup', upKey);

    function downKey(event){
      if(!event.repeat) {
        switch (event.code){
          case controls.PlayerOneAttack:
          if(!playerOne.statusBlock) {
            if(!playerTwo.statusBlock){
              makeAttack(playerOne, playerTwo);
            }
            else { message_right.innerHTML = 'Block!'; }
            }else {
              message_left.innerHTML = `Don't use block and attack together!`;
            }
            break;
          case controls.PlayerOneBlock:
            playerOne.statusBlock = true;
            break;
          case controls.PlayerTwoAttack:
          if(!playerTwo.statusBlock) {
            if(!playerOne.statusBlock){
              makeAttack(playerTwo, playerOne);
            }
            else { message_left.innerHTML = 'Block!'; }
            } else {
              message_right.innerHTML = `Don't use block and attack together!`
            }
            break;
          case controls.PlayerTwoBlock:
            playerTwo.statusBlock = true;
            break;
        }

        comboCheck(playerOne,playerTwo,event);
        comboCheck(playerTwo,playerOne,event);

      }
    }
    function upKey(event) {
      message_right.innerHTML = '';
      message_left.innerHTML = '';
      if(event.code === controls.PlayerOneBlock){
        playerOne.statusBlock = false;
      }
      if(event.code === controls.PlayerTwoBlock){
        playerTwo.statusBlock = false;
      }
    }
    function changeHealthIndicator(defender, damage) {
      defender.indicator = defender.health*defender.indicator/(defender.health + damage);
      if(defender.indicator < 0) {
        defender.indicator = 0;
      }
      document.getElementById(`${defender.position}-fighter-indicator`).style.width = defender.indicator+'px';
    }
    function makeAttack(attacker, defender){
      const damage = getDamage(attacker, defender);
      defender.health -= damage;
      changeHealthIndicator(defender, damage);
      if(defender.position === 'left'){
        message_left.innerHTML = 'Damage: -' + Math.round(damage*100)/100;
      }
      else { message_right.innerHTML = 'Damage: -' + Math.round(damage*100)/100; }
      if(defender.health<=0){
        document.removeEventListener('keydown', downKey);
        document.removeEventListener('keyup', upKey);
        resolve(attacker);
      }
    }
    function comboCheck(attacker, defender, event) {
      
        if (attacker.combo.length !== 0 && !attacker.criticalHitCombination.includes(event.code)) {
          attacker.combo = [];
        }
        if (attacker.criticalHitCombination.includes(event.code) && !attacker.combo.includes(event.code)) {
          attacker.combo.push(event.code);
        }
        if (attacker.combo.length === 3) {
        if(Date.now() - attacker.timeCombo >= 10000 ) {
          criticalHit(attacker, defender);
          }
          else {
            const msg = 'Wait for combo ' + (10000 - (Date.now() - attacker.timeCombo))/1000 + ' sec!';
            if(defender.position === 'left'){
              message_right.innerHTML = msg;
            }
            else { message_left.innerHTML = msg; }
          }
        }
      
    }
    function criticalHit(attacker, defender) {
      const damage = 2 * attacker.attack;
      defender.health -= damage;
      attacker.timeCombo = Date.now();
      changeHealthIndicator(defender, damage);
      if(defender.position === 'left'){
        message_left.innerHTML = 'Combo: -' + damage;
      }
      else { message_right.innerHTML = 'Combo: -' + damage; }
    }
  });
}





export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if(damage > 0) {
    return damage;
  }
  else return 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack*criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense*dodgeChance;
}
