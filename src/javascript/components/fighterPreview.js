import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const person = {...fighter};
  const keys = Object.keys(person);
  for (let i = 0, len = keys.length; i < len; i++) {
    if (keys[i] === '_id') {
      continue;
    }
    let fighterInfo;
    if(keys[i] === 'source'){
      fighterInfo = createElement({
        tagName: 'img',
        className: 'fighter-preview___img'
      });
      fighterInfo.src = person[keys[i]];
      if(position === 'right') {
        fighterInfo.style.transform = 'scale(-1, 1)';
      }
    }
    else {
      fighterInfo = createElement({
        tagName: 'p',
        className: 'fighter-preview___info arena___fighter-name'
      });
      fighterInfo.innerHTML = keys[i].charAt(0).toUpperCase()+keys[i].substr(1) + ': ' + person[keys[i]];
    }
    fighterElement.append(fighterInfo);

  }


  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
